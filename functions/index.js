const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

exports.addCandidate = functions.https.onRequest(async (req, res) => {
  const { email, postalCode, fullName, dateOfBirth } = req.body;

  const ageInMilliseconds = Date.now() - new Date(dateOfBirth).getTime();
  const age = Math.floor(ageInMilliseconds / (1000 * 60 * 60 * 24 * 365));

  const writeResult = await admin
    .firestore()
    .collection('candidates')
    .add({ email, postalCode, fullName, dateOfBirth, age });

  res.json({ result: `Candidate with ID: ${writeResult.id} added.` });
});
