import PropTypes from 'prop-types';
import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const FilterList = ({ items, emptyComponent }) => {
  const [listItems, setListItems] = useState(items);

  const filterItems = event => {
    const value = event.target.value.toLowerCase();

    setListItems(() => {
      const newCandidates = items.filter(
        item =>
          item.email.toLowerCase().includes(value) ||
          item.fullName.toLowerCase().includes(value)
      );

      return newCandidates;
    });
  };

  const listComponent = listItems.map(item => (
    <List key={item.email}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar />
        </ListItemAvatar>
        <ListItemText
          primary={item.fullName}
          secondary={
            <>
              <Typography component="span" variant="body2" color="textPrimary">
                {item.email}
              </Typography>
              {' - '}
              {item.age}
              {' years old'}
            </>
          }
        />
      </ListItem>
    </List>
  ));

  return (
    <>
      <TextField
        style={{ margin: '1rem 0' }}
        id="input-with-icon-textfield"
        label="Search..."
        onChange={filterItems}
        fullWidth
      />
      {listItems.length > 0 ? listComponent : emptyComponent}
    </>
  );
};

FilterList.propTypes = {
  items: PropTypes.array,
  emptyComponent: PropTypes.node,
};

export default FilterList;
