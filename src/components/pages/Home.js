import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';

import FilterList from '../FilterList';
import withAuthorization from '../auth/withAuthorization';
import { db } from '../../firebase';

const HomePage = () => {
  const [candidates, setCandidates] = useState(null);

  useEffect(() => {
    db.getAllCandidates().then(setCandidates);
  }, []);

  if (candidates === null) {
    return <CircularProgress />;
  }

  return (
    <Container maxWidth="sm">
      <FilterList
        items={candidates}
        emptyComponent={<p>No candidates!</p>}
      ></FilterList>
    </Container>
  );
};

HomePage.propTypes = {
  authUser: PropTypes.object.isRequired,
};

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(HomePage);
